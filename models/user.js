const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String, // encrypted using bcrypt plugin
        required: true
    }
});

module.exports = mongoose.model('User', userSchema)

// class User {
//     constructor(username, email){
//         this.name = username;
//         this.email = email;
//         this.password = password
//     }

//     save(){
//         const db = getDb();
//         return db.collection('users').insertOne(this)
//     }
// }
