const username = document.getElementById('navUsername').innerText;

const messageContainer = document.getElementById('messageContainer');
const messageForm = document.getElementById('messageForm');
const messageInput = document.getElementById('messageInput');

messageForm.addEventListener('submit', function (event) {
    event.preventDefault() // ensure that the clicked anchor wont take them to a new url

    const message = messageInput.value;

    // clears the message box
    messageInput.value = '';

    chat(username, message, true);
    socket.emit('chat message', message); // send to the server the message
});

socket.on('chat message', (chatData) => {
    // the server will send a message back and pass to the chat function
    chat(chatData.name, chatData.message, false);
});

function chat(name, message, yourMessage) {
    // depending if its the senders or receiver message will change color.
    let colour = yourMessage == true ? 'red' : 'blue';
    messagesContainer.innerHTML += `<div class="chat-message"><span style="color:${colour}; font-weight: bold;">${name}:</span> <span class="msg">${message}</span></div>`
}
