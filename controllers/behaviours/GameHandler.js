const Game = require('../../models/game');

class GameHandler {
    constructor(gameName, io) {
        this._numberOfConnectedUsers = this._numberOfConnectedUsers.bind(this);
        this._addUserToGame = this._addUserToGame.bind(this);
        this._removeUserFromGame = this._removeUserFromGame.bind(this);
        this.gameInteractionSetup = this.gameInteractionSetup.bind(this);
        this.io = io;
        this.gameName = gameName;
        this.users = {};
    }

    _numberOfConnectedUsers() {
        return Object.getOwnPropertyNames(this.users).length
    }

    _addUserToGame(socketId, username) {
        this.users[socketId] = username;
    }

    _removeUserFromGame(socketId) {
        delete this.users[socketId];
    }

    gameInteractionSetup(socket) {
        socket.on('login', (username) => {
            console.log('logged in' + username);
            this._addUserToGame(socket.id, username);
        });

        // Drawing
        socket.on('mouseDown', ([x, y]) => socket.broadcast.emit('mouseDown', [x, y]))
        socket.on('mouseMove', ([x, y]) => socket.broadcast.emit('mouseMove', [x, y]))
        socket.on('mouseUp', () => socket.broadcast.emit('mouseUp'))
        socket.on('clear', () => socket.broadcast.emit('clear'))
        socket.on('undo', () => socket.broadcast.emit('undo'))
        socket.on('setColor', (c) => socket.broadcast.emit('setColor', c))
        socket.on('setThickness', (r) => socket.broadcast.emit('setThickness', r))

        socket.on('chat message', (message) => {
            console.log('msg', message);

            socket.broadcast.emit('chat message', {
                name: this.users[socket.id],
                message: message
            })
        });

        // removes the game if the room is empty
        socket.on('disconnect', () => {
            console.log(this.users);
            this._removeUserFromGame(socket.id);
            if (this._numberOfConnectedUsers() === 0) {
                console.log('removing ' + this.gameName);
                Game.deleteOne({ gameName: this.gameName }).exec();
                delete this.io.nsps[this.gameName];
            }
        });
    }
}

module.exports = GameHandler;
