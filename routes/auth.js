const express = require('express');
const router = express.Router();

const authController = require('../controllers/auth');
const isLoggedIn = require('../middleware/isLoggedIn');

router.get('/login', isLoggedIn, authController.getLogin);

router.post('/login', isLoggedIn, authController.postLogin);

router.get('/signup', isLoggedIn, authController.getSignup);

router.post('/signup', isLoggedIn, authController.postSignup);

router.post('/guest', authController.postGuest);

router.post('/logout', authController.postLogout);

module.exports = router;
