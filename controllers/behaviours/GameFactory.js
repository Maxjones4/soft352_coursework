const GameHandler = require('./GameHandler');

class GameFactory {
    constructor(roomNamePrefix, io) {
        this.roomNamePrefix = roomNamePrefix;
        this.io = io;
    }

    createNewGame(gameName) {
        let gameHandler = new GameHandler(gameName, this.io);
        this.io.of(this.roomNamePrefix + gameName).on('connection', function(socket) {
            gameHandler.gameInteractionSetup(socket);
        });
    }
}

module.exports = function(roomPrefix, io) { return new GameFactory(roomPrefix, io) };
