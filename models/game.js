const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const gameSchema = new Schema({
    gameName: {
        type: String,
        required: true
    },
    gameGrade: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('game', gameSchema)
