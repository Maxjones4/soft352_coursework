exports.getGame = (req, res, next) => {
    res.render('layouts/game', {
        username: req.session.username,
        guestUsername: req.session.guestUsername
    });
};
