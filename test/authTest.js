const assert = require('chai').assert;
const auth = require('../controllers/auth');
const httpMocks = require('node-mocks-http');
const User = require('../models/user');
const mongoose = require('mongoose');
const MONGODB_URI = 'mongodb+srv://max:gkQNZKjdnYJfWaQN@soft352-cluster0-uhqe4.mongodb.net/SOFT352?retryWrites=true';
mongoose.connect(MONGODB_URI, { useNewUrlParser: true });

const email = 'MochaTest@testaccount.com';
const username = 'MochaTest123';
const password = 'MochaTest123!';

describe('Authentication Controllers', function() {
    // after(async function() { await remove() });

    it('Testing the POST/signup controller', async function() {
        var calledRender = false; // success first
        let req = { body: { email: email, username: username, password: password }, flash: function(errors, message) { /* Do nothing */ } }
        let res = {
            redirect: function(location) {
                assert(location !== '/login', 'managed to create user that already exists');
            },
            render: function(location) {
                calledRender = true; // Assert cannot go here because of the catch in the method postSignup.
            }
        };

        await auth.postSignup(req, res);
        assert(calledRender, 'render was not called which means this has created a user');
    }); // must always go first

    it('Testing the POST/postLogin controller', async function() {
        var notCalledRedirect = true; // success first
        let req = { body: { username: username, password: password }, flash: function(errors, message) { /* Do nothing */ }, session: {} }
        let res = {
            redirect: function() {
                notCalledRedirect = false; // Assert cannot go here because of the catch in the method postLogin.
            },
            render: function(location) {
                assert(location === 'layouts/home', 'did not render layouts/home');
            }
        };

        await auth.postLogin(req, res);
        assert(notCalledRedirect, 'redirect was called which means this has failed');
    });
});

function remove() {
    return User.deleteOne({ email: email });
}
