const game = require('../models/game');

exports.getHome = (req, res, next) => {
    res.render('layouts/home', {
        username: req.session.username,
        guestUsername: null,
        isLoggedIn: req.session.isLoggedIn
    })
};

exports.getGames = (req, res) => {
    // go into the games collection and get all the games return them via the res obj

    game.find({}).then(results => {
        var response = {};
        for (var i = 0; i < results.length; i++) {
            var result = results[i];
            response[result.gameName] = result.gameGrade;
        }
        res.send(response);
    })
};

exports.getRandomGame = (req, res) => {
    // go into the db get all the games return them via the res obj
    game.find({}).then(results => {
        var response = {};
        var randomGame = Math.floor(Math.random() * results.length); // randomly pick a game
        res.send(results[randomGame].gameName);
    })
};
