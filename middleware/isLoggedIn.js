/**
 * This is used to locked down my endpoints depending if the user is logged in,
 * if the user is logged in then they cant go back to the signin or registration page
 */
module.exports = (req, res, next) => {
    if (req.session.isLoggedIn == true) {
        return res.redirect('/');
    }
    // called next to ensure it moves onto the thing in the route
    next();
}
