const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const errorController = require('./controllers/error');
const flash = require('connect-flash');
const csrf = require('csurf');

const MONGODB_URI = 'mongodb+srv://max:gkQNZKjdnYJfWaQN@soft352-cluster0-uhqe4.mongodb.net/SOFT352?retryWrites=true';

const app = express();
const handleGame = require('./controllers/Behaviours/GameHandler');
// used for the session collection within mongodb,
// chose to store it on the db, but should have done it on server session storage
const store = new MongoDBStore({
    uri: MONGODB_URI,
    collection: 'session'
});
const csrfProtection = csrf();

app.set('views', path.join(__dirname, 'views')); // this tells express where our views can be found
app.set('view engine', 'ejs'); // tells express we are using ejs templating engine

const homeRoute = require('./routes/home');
const gameRoute = require('./routes/game');
const authRoutes = require('./routes/auth');
const Game = require('./models/game');

// allows the use of static files to the client via the public folder
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')))
app.use(
    session({ secret: 'my secret', resave: false, saveUninitialized: false, store: store })
)

// allows for validation error messages to persist beyond the redirects/renders I use
app.use(flash());

app.use(csrfProtection);
app.use((req, res, next) => {
    res.locals.csrfToken = req.csrfToken();
    next();
})

app.use(homeRoute);
app.use(gameRoute);
app.use(authRoutes);

// Anything hits this, means I dont have a route for it, so render 404 page.
app.use(errorController.get404);

// mongoose connection
mongoose
    .connect(MONGODB_URI, { useNewUrlParser: true })
    .then(result => {
        const server = app.listen(8080); // this returns a sever

        const io = require('socket.io')(server); // socket sits on the server
        const gameFactory = require('./controllers/behaviours/GameFactory')('/room/', io)
        console.log('listening on port 8080');

        // get all the games and create the namespace
        Game.find({}).then(results => {
            for (var i = 0; i < results.length; i++) {
                var result = results[i];
                gameFactory.createNewGame(result.gameName);
            }
        });

        io.on('connection', socket => {
            console.log('New Connection: ' + socket.id);

            socket.on('room created', function (data) {
                let lobbyName = data.lobbyName;
                let lobbyGrade = data.lobbyGrade;

                const game = new Game({
                    gameName: lobbyName,
                    gameGrade: lobbyGrade
                });

                game.save();
                console.log('game ' + lobbyName + ' saved');
                gameFactory.createNewGame(lobbyName);
            })
        });
    })
    .catch(err => {
        console.log(err);
    })
