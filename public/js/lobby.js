window.onload = function() {
    var socket = io();

    $('#roomForm').submit(function() {
        const lobbyName = document.getElementById('lobbyName').value;
        const lobbyGrade = document.getElementById('lobbyGrade').value;

        var data = { lobbyName, lobbyGrade }
        socket.emit('room created', data);

        // pass in orig user created lobby name to ensure url is valid
        let newPathName = createLobbyPath(lobbyName);
        let messagesContainer = document.getElementById('lobbyplaceholder');
        messagesContainer.innerHTML += `<button type="button" class="btn btn-primary btn-lg btn-block lobby_button" onclick="location.href = '/room/${newPathName}';" style="margin: 0"><span style="float:left; float: left;clear: left; letter-spacing:1.5px">${lobbyName}</span><span style="float: left;clear: left; font-size: 12px; letter-spacing:0.8px;">${lobbyGrade} </span>`;
        window.location.href = `/room/${newPathName}`;

        return false;
    });

    $('#quickJoin').click(function() {
        $.get('http://localhost:8080/games/random', function(data) {
            const newPathName = createLobbyPath(data);
            // direct users to the new random selected lobby
            window.location.href = `/room/${newPathName}`
        });
    })

    // gets all available games created by users and populates the lobby page
    $.get('http://localhost:8080/games/available', function(data) {
        // console.log(data);
        for (var game in data) {
            const messagesContainer = document.getElementById('lobbyplaceholder');
            const newPathName = createLobbyPath(game); // ensure a valid url

            messagesContainer.innerHTML += `<button type="button" class="btn btn-primary btn-lg btn-block lobby_button" onclick="location.href = '/room/${newPathName}';" style="margin: 0"><span style="float:left; float: left;clear: left; letter-spacing:1.5px">${game}</span><span style="float: left;clear: left; font-size: 12px; letter-spacing:0.8px;">${data[game]} </span>`;
        }
    });

    function createLobbyPath(origName) {
        // if a user enters a game name with spaces then replaces a space with a hyphen to allow a valid url
        return origName.replace(/ /g, '-');
    }
}
