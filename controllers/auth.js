const bcrypt = require('bcryptjs'); // encrypting password

const User = require('../models/user');

exports.postLogin = (req, res, next) => {
    const username = req.body.username;
    const password = req.body.password;

    return User
        .findOne({ username: username })
        .then(user => {
            if (!user) {
                console.log('no user found');
                req.flash('error', 'Invalid username or password');
                res.redirect('/login');
            } else {
                bcrypt
                    .compare(password, user.password) // compares the hashed password to the db password
                    .then(result => {
                        if (result) {
                            console.log('successful login');
                            // add the details to the session collection in mongo
                            req.session.username = username;
                            req.session.isLoggedIn = true;
                            res.render('layouts/home', {
                                username: req.session.username,
                                guestUsername: null,
                                isLoggedIn: true
                            });
                        } else {
                            // redirect the user back to the login page to sign in
                            res.redirect('/login');
                        }
                    })
                    .catch(err => {
                        console.log(err);
                        res.redirect('/login');
                    })
            }
        })
};

exports.getLogin = (req, res, next) => {
    res.render('layouts/login', {
        errorMessage: req.flash('error')
    });
};

exports.getSignup = (req, res, next) => {
    res.render('layouts/signup', {
        errorMessage: req.flash('error')
    });
};

exports.postSignup = (req, res, next) => {
    // gets the details out of the body of the post
    const username = req.body.username;
    const email = req.body.email;
    const password = req.body.password;

    return User
        .findOne({ username: username })
        .then(userDoc => {
            if (userDoc) {
                console.log('Error, the email already exists');
                res.render('/signup', {
                    errorMessage: req.flash('error', 'email already in use')
                });
            } else {
                bcrypt
                    .hash(password, 12)// hash password 12 rounds of salt recommended by bcrypt docs
                    .then(hashedPassword => {
                        const user = new User({
                            username: username,
                            email: email,
                            password: hashedPassword
                        });
                        console.log('user saved');
                        user.save().then(result => {
                            res.redirect('/login');
                        });
                    });
            }
        })
        .catch(err => {
            console.log(err);
            res.redirect('/signup');
        });
};

exports.postGuest = (req, res, next) => {
    // add the users details to the session collection in mongo
    let guestUsername = req.body.guestUsername;
    req.session.guestUsername = guestUsername;
    req.session.isLoggedIn = true;

    console.log(guestUsername);
    res.render('layouts/home', {
        guestUsername: guestUsername,
        isLoggedIn: true
    })
}

// destroy the session, built in method by express-session
exports.postLogout = (req, res, next) => {
    req.session.destroy(() => {
        console.log('session deleted from db');
        res.redirect('/login');
    })
};
