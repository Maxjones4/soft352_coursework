const express = require('express');
const router = express.Router();

const homeController = require('../controllers/home');

router.get('/', homeController.getHome);

router.get('/games/available', homeController.getGames);

router.get('/games/random', homeController.getRandomGame)

module.exports = router;
